import React from "react";

export default function BackingFacet({ handleFilterClick, productBacking }) {
  function sortObject(obj) {
    return Object.keys(obj)
      .sort()
      .reduce((a, v) => {
        a[v] = obj[v];
        return a;
      }, {});
  }
  productBacking = sortObject(productBacking);
  return (
    <div class="facet-wrap facet-display">
      <strong>Backing</strong>
      <div className="facetwp-facet">
        {Object.keys(productBacking).map((backing, i) => {
          if (backing && productBacking[backing] > 0) {
            return (
              <div>
                <span
                  id={`backing-filter-${i}`}
                  key={i}
                  data-value={`${backing.toLowerCase()}`}
                  onClick={(e) =>
                    handleFilterClick("backing_facet", e.target.dataset.value)
                  }>
                  {" "}
                  {backing} {` (${productBacking[backing]}) `}
                </span>
              </div>
            );
          }
        })}
      </div>
    </div>
  );
}
